package com.fived.sparkparquetaws;

import com.fived.sparkcommon.PropertyUtil;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;

/**
 * Created by klocek on 06.11.18.
 */
public class Application {

    public static final String FILE_SELF_CLOSING_REMOVED_S3_PATH_PARAM_NAME = "fileSelfClosingRemovedS3Path";
    static String FILE_PARQUET_S3_PATH_PARAM_NAME = "fileParquetS3Path";

    public static void main(String[] args) throws Exception {
        PropertyUtil propertyUtil = new PropertyUtil();
        makeParquet(propertyUtil);
    }

    private static void makeParquet(PropertyUtil propertyUtil) {
        SparkSession sc = SparkSession
                .builder()
                .appName("SparkAws-parquet")
                .getOrCreate();
        SQLContext sqlContext = new SQLContext(sc);
        Dataset df = sqlContext.read()
                .format("xml")
                .option("rowTag", "row")
                .load(propertyUtil.getProperty(FILE_SELF_CLOSING_REMOVED_S3_PATH_PARAM_NAME));
        Dataset df2 = df.drop("dummy");
        df2.write().parquet(propertyUtil.getProperty(FILE_PARQUET_S3_PATH_PARAM_NAME));
    }
}
