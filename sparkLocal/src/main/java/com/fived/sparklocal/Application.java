package com.fived.sparklocal;

import com.fived.sparkcommon.PropertyUtil;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;

/**
 * Created by klocek on 06.11.18.
 */
public class Application {

    public static final String FILE_RAW_PATH_PARAM_NAME = "fileRawPath";
    public static final String FILE_SELF_CLOSING_REMOVED_PATH_PARAM_NAME = "fileSelfClosingRemovedPath";
    public static final String FILE_PARQUET_PATH_PARAM_NAME = "fileParquetPath";
    public static String tag = "/>";
    public static String tagToAdd = "><dummy>dummy</dummy></row>";

    public static void main(String[] args) throws Exception {
        PropertyUtil propertyUtil = new PropertyUtil();
//        prepare(propertyUtil);
        makeParquet(propertyUtil);
    }

    private static void prepare(PropertyUtil propertyUtil) throws Exception {
        JavaSparkContext sc = new JavaSparkContext(new SparkConf().setAppName("SparkLocal-self-closing-tags-workaround")
                .setMaster("local"));
        JavaRDD<String> lines = sc.textFile(propertyUtil.getProperty(FILE_RAW_PATH_PARAM_NAME));
        JavaRDD<String> linesNewRDD = lines
                .filter(line -> line.trim().startsWith("<row"))
                .map(line -> line.replaceAll(tag, tagToAdd));
        linesNewRDD.saveAsTextFile(propertyUtil.getProperty(FILE_SELF_CLOSING_REMOVED_PATH_PARAM_NAME));

    }

    private static void makeParquet(PropertyUtil propertyUtil) {
        SparkSession sc = SparkSession
                .builder()
                .appName("SparkLocal-to-parquet")
                .master("local")
                .getOrCreate();
        SQLContext sqlContext = new SQLContext(sc);
        Dataset df = sqlContext.read()
                .format("xml")
                .option("rowTag", "row")
                .load(propertyUtil.getProperty(FILE_SELF_CLOSING_REMOVED_PATH_PARAM_NAME));
        Dataset df2 = df.drop("dummy");
        df2.write().parquet(propertyUtil.getProperty(FILE_PARQUET_PATH_PARAM_NAME));
    }
}
