package com.fived.sparkcommon;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by klocek on 07.11.18.
 */
public class PropertyUtil {

    private String configFileName = "config.properties";
    private Properties properties;

    public PropertyUtil() throws Exception{
        properties = new Properties();
        InputStream input = null;

        try {
            input = PropertyUtil.class.getClassLoader().getResourceAsStream(configFileName);
            properties.load(input);

        } finally {
            if (input != null) {
                try {
                    input.close();
                }catch (IOException e){
                    throw e;
                }
            }
        }
    }

    public String getProperty(String propertyName){
        return properties.getProperty(propertyName);
    }
}
