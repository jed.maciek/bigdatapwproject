package com.fived.sparkformatdataaws;

import com.fived.sparkcommon.PropertyUtil;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * Created by klocek on 06.11.18.
 */
public class Application {

    public static final String FILE_RAW_S3_PATH_PARAM_NAME = "fileRawS3Path";
    public static final String FILE_SELF_CLOSING_REMOVED_S3_PATH_PARAM_NAME = "fileSelfClosingRemovedS3Path";
    public static String tag = "/>";
    public static String tagToAdd = "><dummy>dummy</dummy></row>";

    public static void main(String[] args) throws Exception {
        PropertyUtil propertyUtil = new PropertyUtil();
        prepare(propertyUtil);
    }

    private static void prepare(PropertyUtil propertyUtil) throws Exception {
        JavaSparkContext sc = new JavaSparkContext(new SparkConf().setAppName("SparkAws-self-closing-tags-workaround"));
        JavaRDD<String> lines = sc.textFile(propertyUtil.getProperty(FILE_RAW_S3_PATH_PARAM_NAME));
        JavaRDD<String> linesNewRDD = lines
                .filter(line -> line.trim().startsWith("<row"))
                .map(line -> line.replaceAll(tag, tagToAdd));
        linesNewRDD.saveAsTextFile(propertyUtil.getProperty(FILE_SELF_CLOSING_REMOVED_S3_PATH_PARAM_NAME));
    }

}
